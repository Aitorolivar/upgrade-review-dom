// 1.1 Basandote en el array siguiente, crea una lista ul > li 
// dinámicamente en el html que imprima cada uno de los paises.
// const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const countries = ["Japón", "Nicaragua", "Suiza", "Australia", "Venezuela"];

const $$ul = document.createElement("ul");
document.body.appendChild($$ul);

for (let countri of countries) {
  const $$li = document.createElement("li");
  $$li.textContent = countri;
  $$ul.appendChild($$li);
};

// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.
const $$paragraph = document.querySelector(".fn-remove-me");
$$paragraph.remove();

//1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
// en el div de html con el atributo data-function="printHere".
// const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];>


const cars = ["Mazda 6", "Ford fiesta", "Audi A4", "Toyota corola"];

const $$div = document.querySelector('[data-function="printHere"]');
const $$ulTwo = document.createElement("ul");
$$div.appendChild($$ulTwo);

for (let car of cars) {
  const $$li = document.createElement("li");
  $$li.innerHTML = car;
  $$ulTwo.appendChild($$li);
};

// 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
// h4 para el titulo y otro elemento img para la imagen.
// const countries = [
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
// ];


const countries2 = [
  { title: "Random title-1", imgUrl: "https://picsum.photos/300/200?random=1" },
  { title: "Random title-2", imgUrl: "https://picsum.photos/300/200?random=2" },
  { title: "Random title-3", imgUrl: "https://picsum.photos/300/200?random=3" },
  { title: "Random title-4", imgUrl: "https://picsum.photos/300/200?random=4" },
  { title: "Random title-5", imgUrl: "https://picsum.photos/300/200?random=5" },
];


for(let countrie of countries2){
    const $$div = document.createElement('div');
    document.body.appendChild($$div);
    const $$h4Title = document.createElement('h4');
    $$h4Title.innerHTML = countrie.title;
    $$div.appendChild($$h4Title);
    const $$h4Image = document.createElement('img');
    $$h4Image.src = countrie.imgUrl;
    $$div.appendChild($$h4Image);
};

// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último elemento de la lista.


const $$btn = document.createElement('button');
$$btn.classList.add('b-btn');
$$btn.innerHTML = 'Delete last';
document.body.appendChild($$btn);
$$btn.addEventListener('click', removeImage);
function removeImage(){
    const $$lastImg = document.querySelector('[src="https://picsum.photos/300/200?random=5"]');
    $$lastImg.remove();
    const $$span = document.createElement('span');
    $$span.innerHTML = 'Deleted';
    const $$div = document.querySelectorAll('div');
    $$div[5].appendChild($$span).style = 'font-size: 60px';
};

// 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los elementos de las listas que elimine ese mismo elemento del html.

const $$allImg = document.querySelectorAll('img');

let countBtn = 1;
for(let img of $$allImg){
    const $$allButtons = document.createElement('button');
    document.body.appendChild($$allButtons);
    $$allButtons.innerText = `Random ${countBtn}`;
    countBtn++;
    $$allButtons.addEventListener('click', removeImages);
    function removeImages(){
        img.remove();
    };
};


